/******************************************
 *  Author : Author   
 *  Created On : Thu Nov 15 2018
 *  File : esercizio5.js
 *******************************************/
var _MS_PER_DAY = 1000 * 60 * 60 * 24;

function giorniANatale(){
    var dataDiOggi = new Date();
    var natale = new Date("December 25, 2018");
    

    return Math.round( (natale.getTime()-dataDiOggi.getTime()) / _MS_PER_DAY);

}

console.log(giorniANatale());
 