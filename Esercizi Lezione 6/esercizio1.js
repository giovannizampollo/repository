/******************************************
 *  Author : Author   
 *  Created On : Thu Nov 15 2018
 *  File : esercizio1.js
 *******************************************/
function data(){
    //creazione oggetto date

 var data = new Date();
 var giorno = data.getDay();

 //creazione stringa vuota per settare il giorno della settimana

var giornoStringa="";

//controllo del giorno, e assegnazione del giorno della settimana alla stringa

if(giorno==0){
    giornoStringa="Domenica";
}else if (giorno==1){
    giornoStringa="Lunedì";
}else if (giorno==2){
    giornoStringa="Martedì";
}
else if (giorno==3){
    giornoStringa="Mercoledì";
}else if (giorno==4){
    giornoStringa="Giovedì";
}else if (giorno==5){
    giornoStringa="Venerdì";
}
else if (giorno==6){
    giornoStringa="Sabato";
}

//calcolo dei minuti e dell'ora

var ora = data.getHours();
var minuti=data.getMinutes();

var periodo ="";

if (ora>12){
    ora=ora-12;
    periodo="i pomeriggio";
}else if (ora>18){
    ora=ora-12;
    periodo = "ella sera";
}else if (ora>21){
    ora=ora-12;
    periodo="i notte"
}else{
    periodo="el mattino";
}

var risultato="Oggi è "+giornoStringa+" e sono le "+ora+":"+minuti+" d"+periodo;
return risultato;



}
 
//stampa del risultato
console.log(data());
