/******************************************
 *  Author : Author   
 *  Created On : Thu Nov 15 2018
 *  File : esercizio7.js
 *******************************************/

function contaVocali(frase){
    frase.toLowerCase();

    var vett=frase.split("");
    var vocali=0;
    
    for(var i=0; i<vett.length;i++){
        if(vett[i]=="a"|| vett[i]=="e" || vett[i]=="i" || vett[i]=="o" || vett[i]=="u" || vett[i]=="y"|| vett[i]=="ò"|| vett[i]=="à"|| vett[i]=="è"|| vett[i]=="ù"|| vett[i]=="ì")
        {
            vocali++;
        }
    }
    return vocali;
    }

console.log("Numero di vocali: "+contaVocali("Oggi siamo a fare lo stage al Bearzi"));