/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 14 2018
 *  File : classi.js
 *******************************************/
class Being{
    constructor(){
        console.log("I am a being");
    }

    sayHi(){
        console.log("Hi");
    }
}


class Animal extends Being{
    constructor(age){
        super();
        this.age=age;
        this.favouriteFood="";
        this.noise="";
        setTimeout(()=>this.makeNoise(), 1000);
        console.log("I am an animal and I am " + this.age);
    }
    run(){
            console.log("I am running");
    }
    eat(){
        console.log("I eat "+this.favouriteFood)

    }
    makeNoise(){
        console.log("My noise is "+ this.noise);

    }

}

class Dog extends Animal{
    constructor(age){
        super(3);
        this.favouriteFood="Meat";
        this.noise="Bau";
        this._noiseInterval =  setInterval(()=>this.makeNoise(), 2000);
        console.log("I am a dog and I am "+this.age);
    }

    shhht(){
        clearInterval(this._noiseInterval);
    }

}

class Cat extends Animal{
    constructor(age){
        super(2);
        this.favouriteFood="Fish";
        this.noise="Miao";
        console.log("I am a cat and I am "+this.age);
    }

    run(){
        super.run();
        console.log("I run like a cat");
    }

}


var b = new Being();
b.sayHi();
var a = new Animal(5);

console.log("-----------------------------------");
var d=new Dog(2);
d.run();
d.makeNoise();
d.eat();
console.log("-----------------------------------");
var c = new Cat(3);
c.run();
c.makeNoise();
c.eat();
console.log("-----------------------------------");