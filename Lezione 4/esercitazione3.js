/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : esercitazione3.js
 *******************************************/


 //es1 - massimo tra due valori
 console.log("es1 - massimo tra due valori");
var a=5;
var b= 10;
if(a>b){
    console.log("Il maggiore è a= "+a);
}else{
    console.log("Il maggiore è b= "+b);
}

//es 2 - verifica se i numeri siano positivi o negativi
console.log("es 2 - verifica se i numeri siano positivi o negativi");
var ar1=[1,5,-4,-3,2,-1,7];

for(var i =0;i<ar1.length;i++){
    if(ar1[i]>0){
        console.log("positivo e il numero e': "+ar1[i]);
     }else{
        console.log("negativo e il numero e' "+ar1[i]);
     }
}

//es 3 - maggiore di 10 numeri
console.log("es 3 - maggiore di 10 numeri");
var ar2=[0, 7, 14, 5, 6, 24, 100, 173, 76, 102];
var max=ar2[0];
for(var j=0;j<ar2.length;j++){
    if(ar2[j]>max){
        max=ar2[j];
    }
}
console.log("Il massimo e' "+ max);


//es 4 - tabellina del 5
console.log("es 4 - tabellina del 5");

for (var k=0; k<10; k++){
    console.log("5 x "+(k+1)+" = "+5*(k+1) );
}

//es 5 - tabelline da 1 a 10
console.log("es5 - tabellina da 1 a 10");

for(var z=0; z<10;z++){
    console.log('tabellina '+ (z+1));
    for (var w=0; w<10;w++){ 
        console.log((z+1)+" x "+(w+1)+" = "+ (z+1)*(w+1));
    }
}
