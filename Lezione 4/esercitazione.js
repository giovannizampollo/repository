/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : esercitazione.js
 *******************************************/

var var1="Milano";
var var2=8+8/3;
var var3=["uno", 2,3+1];
var var4=undefined;
var var5=null;
var var6="Uno"+2;
var var7=NaN;
var var8="false";
var var9=true;

console.log("\"Milano\" "+typeof(var1));
console.log("8+8/3 "+typeof(var2));
console.log("[\"uno\", 2,3+1] "+typeof(var3));
console.log("undefined "+typeof(var4));
console.log("null "+typeof(var5));
console.log("\"uno\"+2 "+typeof(var6));
console.log("NaN "+typeof(var7));
console.log("\"false\" "+typeof(var8));
console.log("true "+typeof(var9));




