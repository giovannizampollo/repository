/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 17 2018
 *  File : GaraVO.js
 *******************************************/
class GaraVO {
    constructor() {
        /**
         * nel caso arrivi dall'esterno 
         * non ci sarebbe un costruttore,
         * le dichiariamo per avere
         * autocompletamento
         */
        this.atleti = null;
        this.distanza = null;
        this.vincitore = null;
    }
}